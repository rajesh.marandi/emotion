
# coding: utf-8

# In[8]:


#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 22 15:26:41 2019

@author: atul
"""
import json

import cv2
import numpy as np
import pandas as pd
import tensorflow as tf

from emotion_detection.emotion.data_generator import BatchGenerator
from emotion_detection.emotion.load_model import MODEL
from emotion_detection.emotion.read_data import read_data_from_image_dir
# from emotion_detection.emotion.read_data import read_from_mp4
# from emotion_detection.emotion.visualize import plot
from emotion_detection.facialboundingbox.detect_facebox import detect_face_boxes

# In[1]:


def preprocess_predict(video_data, boxes):
    """
    Preprocess videos by cropping, resizing, color to grayscale and predicting
    """
    cropped_resized_grayscaled_video = []
    _, width, height, _ = video_data.shape
    for i in range(video_data.shape[0]):
        temp = video_data[i][
            int(width * boxes[i][0][0]) : int(width * boxes[i][0][2]),
            int(height * boxes[i][0][1]) : int(height * boxes[i][0][3]),
            :,
        ]
        temp = cv2.resize(temp, (48, 48))
        cropped_resized_grayscaled_video.append(temp)
    cropped_resized_grayscaled_video = np.array(cropped_resized_grayscaled_video)
    cropped_resized_grayscaled_video = tf.image.rgb_to_grayscale(
        cropped_resized_grayscaled_video
    ).eval(session=tf.Session())
    test_generator = BatchGenerator(
        cropped_resized_grayscaled_video, np.ones(video_data.shape[0])
    ).generate(batch_size=1, image_size=160)
    predictions = MODEL.predict_generator(
        test_generator, steps=np.ceil(cropped_resized_grayscaled_video.shape[0] / 1)
    )
    return predictions


def give_emotions_mp4(video_data, plot_im=False, from_array_bgr=False):
    """
    Input video file return prediction probabilities for 7 emmotion classes
    """
#     if not from_array_bgr:
#         video_data = read_from_mp4(video_clip_path_or_array)
#     else:
#         video_data = video_clip_path_or_array
    video_data = cv2.resize(video_data,(224,224)).reshape(1,224,224,3)
    video_data = video_data[:, :, :, ::-1]
    boxes = detect_face_boxes(video_data)
    predictions = preprocess_predict(video_data, boxes)
    columns = ["Angry", "Disgust", "Fear", "Happy", "Sad", "Surprise", "Neutral"]
    predictions_df = pd.DataFrame(predictions, columns=columns)
    predictions_json = json.loads(predictions_df.to_json(orient="records"))
    if plot_im:
        plot(video_data, boxes, predictions_json, gray_scale=False)
    return predictions_json


def give_emotions_images(image_list_dir_or_array, plot_im=False, from_array_bgr=False):
    """
    Input video file return prediction probabilities for 7 emmotion classes
    """
    if not from_array_bgr:
        video_data = read_data_from_image_dir(image_list_dir_or_array)
    else:
        video_data = image_list_dir_or_array
    video_data = video_data[:, :, :, ::-1]
    boxes = detect_face_boxes(video_data)
    predictions = preprocess_predict(video_data, boxes)
    columns = ["Angry", "Disgust", "Fear", "Happy", "Sad", "Surprise", "Neutral"]
    predictions_df = pd.DataFrame(predictions, columns=columns)
    predictions_json = json.loads(predictions_df.to_json(orient="records"))
    if plot_im:
        plot(video_data, boxes, predictions_json, gray_scale=False)
    return predictions_json

## Example
# import cv2
# img_c=cv2.imread('frame1.jpg')
# emotion_list = give_emotions_mp4(img_c)
# print(emotion_list[0])

#print(emotion_list)

