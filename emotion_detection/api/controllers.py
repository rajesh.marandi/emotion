#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 22 17:26:44 2019

@author: atul
"""
import json

from flask import request

from emotion_detection import APP
from emotion_detection.emotion.preprocess_predict import give_emotions_mp4

@APP.route("/health_check")
def health_check():
    """
    Health check
    """
    health_check_json = {"api": "emotion_classification", "status": 200}
    return json.dumps(health_check_json)


@APP.route("/emotion_classification", methods=["POST"])
def reco_similar_profile():
    """
    App Definition
    """
    response = {
        "status_code": 500,
        "message": "Failed to return similar profile",
        "data": None,
    }
    try:
        print(request.args.get("video_path"),'jjjjjjjjjjjjjjjjjjjjjjjjjjjjjj')
        video_path = request.args.get("video_path")
#        request_json = request.get_json(silent=True, force=True)
#        print(request_json)
        result = give_emotions_mp4(video_path)
        print(result)
        response["status_code"] = 200
        response["message"] = "emotion classification successful"
        response["data"] = result
    except Exception as e:
        print("Exception : {}".format(str(e)))
        response["message"] = str(e)
    return json.dumps(response)


give_emotions_mp4('/home/quantiphi/Rajesh_isc/Rajesh_backup/ischoolconnect/isc_visa_emotion_detection_raj/sample.mp4')
