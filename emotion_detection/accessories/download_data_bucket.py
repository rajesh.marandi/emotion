#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  8 12:20:50 2019

@author: atul
"""
import os

from google.cloud import storage

from config import GOOGLE_APPLICATION_CREDENTIALS

# from config import BASE_DIR

os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = GOOGLE_APPLICATION_CREDENTIALS


def download_blob(bucket_name, source_blob_name, destination_file_name):
    """
    Downloads a blob from the bucket.
    """
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.blob(source_blob_name)

    blob.download_to_filename(destination_file_name)

    print("Blob {} downloaded to {}.".format(source_blob_name, destination_file_name))


# download_blob("reco_similar_profile_data","data_similarity.csv",BASE_DIR+"/data_similarity.csv")
# download_blob("reco_similar_profile_data","reco_data_yocket_similarity.csv",BASE_DIR+"/reco_data_yocket_similarity.csv")


def create_bucket(bucket_name):
    """
    Creates a new bucket.
    """
    storage_client = storage.Client()
    bucket = storage_client.create_bucket(bucket_name)
    print("Bucket {} created".format(bucket.name))


# create_bucket("visa_emotion_data")


def upload_blob(bucket_name, source_file_name, destination_blob_name):
    """
    Uploads a file to the bucket.
    """
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.blob(destination_blob_name)

    blob.upload_from_filename(source_file_name)

    print("File {} uploaded to {}.".format(source_file_name, destination_blob_name))


# upload_blob("visa_emotion_data","/home/atul/ML/Visa_Emotion_Detection/facenet_model.json","facenet_model.json")
# upload_blob("visa_emotion_data","/home/atul/ML/Visa_Emotion_Detection/frozen_inference_graph_face.pb","frozen_inference_graph_face.pb")
# upload_blob("visa_emotion_data","/home/atul/ML/Visa_Emotion_Detection/balanced_class_model_transfer.h5","balanced_class_model_transfer.h5")


def download_in_dir(bucket_name, file_name, directory_to_save):
    """
    Download to desired directory
    """
    if os.path.exists(directory_to_save + "/" + file_name):
        pass
    else:
        print("downloading: ", file_name)
        download_blob(bucket_name, file_name, directory_to_save + "/" + file_name)
