#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 22 17:35:42 2019

@author: atul
"""
import tensorflow as tf
from flask import Flask
from flask_cors import CORS

from config import BASE_DIR
from emotion_detection.accessories.download_data_bucket import download_in_dir

APP = Flask(__name__)
APP.config.from_object("config")
CORS(APP)

download_in_dir("visa_emotion_data", "facenet_model.json", BASE_DIR)
download_in_dir("visa_emotion_data", "frozen_inference_graph_face.pb", BASE_DIR)
download_in_dir("visa_emotion_data", "balanced_class_model_transfer.h5", BASE_DIR)

PATH_TO_CKPT = BASE_DIR + "/frozen_inference_graph_face.pb"
DETECTION_GRAPH = tf.Graph()
with DETECTION_GRAPH.as_default():
    OD_GRAPH_DEF = tf.GraphDef()
    with tf.gfile.GFile(PATH_TO_CKPT, "rb") as fid:
        SERIALIZED_GRAPH = fid.read()
        OD_GRAPH_DEF.ParseFromString(SERIALIZED_GRAPH)
        tf.import_graph_def(OD_GRAPH_DEF, name="")


from emotion_detection.api.controllers import *
