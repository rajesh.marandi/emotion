#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 22 14:03:13 2019

@author: atul
"""
import keras
from keras.models import model_from_json


BASE_MODEL = model_from_json(open("facenet_model.json", "r").read())
TOP_MODEL = keras.Sequential(
    [
        keras.layers.Dense(128, activation="relu", input_shape=(None, 128)),
        keras.layers.Dense(7, activation="softmax"),
    ]
)
MODEL = keras.Model(inputs=BASE_MODEL.input, outputs=TOP_MODEL(BASE_MODEL.output))
MODEL.load_weights("balanced_class_model_transfer.h5")
MODEL._make_predict_function()
