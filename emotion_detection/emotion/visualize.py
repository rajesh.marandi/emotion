#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 22 15:58:37 2019

@author: atul
"""
import cv2
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf


def plot(video_data, boxes, predictions=None, gray_scale=False):
    """
    plot images with their predicted label for evaluation
    """
    _, width, height, _ = video_data.shape
    # n_columns = 5
    n_rows = int(np.ceil(video_data.shape[0] / 5))
    _, axis = plt.subplots(n_rows, 5, figsize=(15, 5 * n_rows))
    for row in range(n_rows):
        for column in range(5):
            fig_num = row * 5 + column
            if fig_num < video_data.shape[0]:
                temp = video_data[fig_num][
                    int(width * boxes[fig_num][0][0]) : int(
                        width * boxes[fig_num][0][2]
                    ),
                    int(height * boxes[fig_num][0][1]) : int(
                        height * boxes[fig_num][0][3]
                    ),
                    :,
                ]
                if gray_scale:
                    temp = tf.image.rgb_to_grayscale(cv2.resize(temp, (48, 48))).eval(
                        session=tf.Session()
                    )
                    axis[row, column].imshow(temp[:, :, 0], cmap="gray")
                else:
                    axis[row, column].imshow(temp)
                if predictions is None:
                    axis[row, column].set_xlabel("xyz")
                else:
                    sorted_d = sorted(
                        (value, key) for (key, value) in predictions[fig_num].items()
                    )
                    show_dict = {
                        pair[1]: np.round(pair[0], 2) for pair in sorted_d[-2:]
                    }
                    axis[row, column].set_xlabel(show_dict)
