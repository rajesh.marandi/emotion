#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 22 13:55:27 2019

@author: atul
"""
import cv2
import numpy as np
from imgaug import augmenters as iaa


class BatchGenerator:
    """
    A generator to generate batches of samples and corresponding labels indefinitely.

    The labels are read from a CSV file.

    Shuffles the dataset consistently after each complete pass.

    Can perform image transformations for data conversion and data augmentation,
    for details please refer to the documentation of the `generate()` method.
    """

    def __init__(self, x_train, y_train):
        self.x_train = x_train
        self.y_train = y_train
        self.shape = self.x_train.shape

    def generate(
        self,
        batch_size=32,
        augment_multiplier=0,
        image_size=160,
        return_grayscale=False,
    ):
        """
        generator function yields images with labels for training and testing
        """
        current = 0
        while True:
            if current > self.shape[0]:
                current = 0
                continue

            batch_x = self.add_channel(self.x_train[current : current + batch_size])
            batch_y = self.y_train[current : current + batch_size]
            if augment_multiplier != 0:
                original_dtype = batch_x.dtype
                batch_x = batch_x.astype("uint8")
                batch_x, batch_y = augment_data(batch_x, batch_y, augment_multiplier)
                batch_x = batch_x.astype(original_dtype)
            batch_x = resize_image(batch_x, image_size)
            if return_grayscale:
                batch_x = np.expand_dims(batch_x[:, :, :, 0], -1)
            current = current + batch_size
            # print(current)
            yield batch_x, batch_y

    def add_channel(self, data):
        """
        add channel to pass as input to facenet
        """
        if self.shape[3] == 1:
            temp = np.tile(data, (1, 1, 1, 3))
        else:
            temp = data
        return temp


def resize_image(data, image_size):
    """
    resizing image
    """
    temp = []
    for i in range(data.shape[0]):
        temp.append(cv2.resize(data[i], (image_size, image_size)))
    return np.array(temp)


# Function to create augmentation, multiplier controls number of augmentation per image
def augment_data(train, label, multiplier=0):
    """
    data augmentation using imgaug library
    """
    seq = iaa.Sequential(
        [
            iaa.Fliplr(0.5),  # horizontal flips
            iaa.Crop(percent=(0, 0.04)),  # random crops
            iaa.ContrastNormalization((0.75, 1.5)),
            iaa.Affine(
                scale={"x": (0.8, 1.2), "y": (0.8, 1.2)},
                translate_percent={"x": (-0.1, 0.1), "y": (-0.1, 0.1)},
                rotate=(-10, 10),
                shear=(-5, 5),
            ),
        ],
        random_order=True,
    )
    image_aug = []
    image_aug = train
    label_mult = label
    if multiplier >= 1:
        for _ in range(multiplier):
            image_aug = np.vstack([image_aug, seq.augment_images(train)])
            label_mult = np.vstack([label_mult, label])
    return image_aug, label_mult
