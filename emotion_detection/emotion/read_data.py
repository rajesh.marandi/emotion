#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 22 15:43:43 2019

@author: atul
"""
import os

import cv2
import numpy as np

W, H = 224, 224


def read_from_mp4(path):
    """
    Read video files and extract frames at some equal intervals
    """
    vid_obj = cv2.VideoCapture(path)
    count = 0
    success = 1
    image_list = []
    while success:
        success, image = vid_obj.read()
        if not success:
            break
        if count % 10 == 0:
            image = cv2.resize(image, (W, H))
            image_list.append(image)
        count += 1
    return np.array(image_list)


def read_data_from_image_dir(image_path_dir):
    """
    Read images from directory
    """
    file_paths = [image_path_dir + "/" + file for file in os.listdir(image_path_dir)]
    video_array_list = [cv2.resize(cv2.imread(path), (W, H)) for path in file_paths]
    return np.array(video_array_list)
