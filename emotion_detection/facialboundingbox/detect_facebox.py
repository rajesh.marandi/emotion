#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 22 15:20:34 2019

@author: atul
"""
import numpy as np
import tensorflow as tf

from emotion_detection import DETECTION_GRAPH


def detect_face_boxes(video_data):
    """
    Takes as input a batch of images and returns bounding boxes, classes, scores for each of them
    """
    with DETECTION_GRAPH.as_default():
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        with tf.Session(graph=DETECTION_GRAPH, config=config) as sess:
            image_tensor = DETECTION_GRAPH.get_tensor_by_name("image_tensor:0")
            boxes = DETECTION_GRAPH.get_tensor_by_name("detection_boxes:0")
            batch_size = 2
            num_batches = int(np.ceil(video_data.shape[0] / batch_size))
            boxes_list = []
            for i in range(num_batches):
                (box) = sess.run(
                    [boxes],
                    feed_dict={
                        image_tensor: video_data[i * batch_size : (i + 1) * batch_size]
                    },
                )
                boxes_list.append(np.squeeze(np.array(box), axis=(0,)))
            boxes_list = np.vstack(boxes_list)
        # ymin,xmin,ymax,xmax
    return boxes_list
