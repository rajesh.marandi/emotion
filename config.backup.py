#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  8 12:20:50 2019

@author: atul
"""


import os

BASE_DIR = os.path.abspath(os.path.dirname(__file__))  
GOOGLE_APPLICATION_CREDENTIALS = BASE_DIR + "/key.json"
PORT = 5000
DEBUG = True
HOST = "0.0.0.0"
